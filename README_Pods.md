# PiOSNetworkClient

[![CI Status](http://img.shields.io/travis/mrcloud/PiOSNetworkClient.svg?style=flat)](https://travis-ci.org/mrcloud/PiOSNetworkClient)
[![Version](https://img.shields.io/cocoapods/v/PiOSNetworkClient.svg?style=flat)](http://cocoapods.org/pods/PiOSNetworkClient)
[![License](https://img.shields.io/cocoapods/l/PiOSNetworkClient.svg?style=flat)](http://cocoapods.org/pods/PiOSNetworkClient)
[![Platform](https://img.shields.io/cocoapods/p/PiOSNetworkClient.svg?style=flat)](http://cocoapods.org/pods/PiOSNetworkClient)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PiOSNetworkClient is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PiOSNetworkClient"
```

## Author

mrcloud, florianp37@me.com

## License

PiOSNetworkClient is available under the MIT license. See the LICENSE file for more info.
