//
//  MoyaNetworkClientError.swift
//  Pods
//
//  Created by Florian PETIT on 28/04/2017.
//
//

import Foundation

/// this enum represents the errors that can occur using the NetworkClient
public enum MoyaNetworkClientError: Swift.Error {
    /**
     JSON Parsing error.
     - parameter cause: cause of the error
     */
    case JSONParsing(cause: Swift.Error)
    
    /**
     Invalid Mapping error.
     - parameter json: the json the mapping intended to use
     */
    case InvalidMapping(json: Any)
    
    /**
     Request error, occurring when the API is returning a statusCode of 4xx
     - parameter json: the json wrapped error
     */
    case RequestError(json: Any)
    
    /**
     Request failure, occurring when the API is returning an error
     - parameter cause: cause of the error
     */
    case RequestFailure(cause: Swift.Error)
}
