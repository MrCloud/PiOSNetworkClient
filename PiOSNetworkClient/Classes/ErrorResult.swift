//
//  ErrorResult.swift
//  Pods
//
//  Created by Florian PETIT on 21/04/2017.
//
//

import Foundation
import ObjectMapper

/**
    This structure is used to wrap errors into ObjectMapper's Mappable objects
 */
public struct ErrorResult: Mappable {
    /// A String that identifies the type of error that occured
    public var error: String?
   
    /// An Int that identifies the code of error that occured
    public var code: Int?
    
    /// A String that identifies the description of error that occured
    public var description: String {
        if let code = code, let error = error {
            return "Error: \(error) Code: \(code)"
        } else if let error = error {
            return "Error: \(error)"
        } else if let code = code {
            return "Unknown error. Code: \(code)"
        } else {
            return "Unknown error"
        }
    }
    
    /**
     ObjectMapper's Mappable initializer.
     - parameter map: mapping
     */
    public init?(map: Map) { }
    
    /**
     ObjectMapper's BaseMappable mapping magic.
     - parameter map: mapping
     */
    public mutating func mapping(map: Map) {
        error <- map[Keys.error]
        code <- map[Keys.code]
    }
    
    /**
     Checks if the error is correctly formed. (i.e: with a code and error)
     - returns: Wether the error is valid or not.
     */
    public func isValidError() -> Bool {
        if let _ = code, let _ = error {
            return true
        }
        return false
    }
}

/**
 This extension is used to wrap static json keys to be mapped into the errors
 */
extension ErrorResult {
    struct Keys {
        static let error = "error"
        static let code = "code"
    }
}
