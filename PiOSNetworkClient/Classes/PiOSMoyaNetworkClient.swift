//
//  PiOSMoyaNetworkClient.swift
//  Pods
//
//  Created by Florian PETIT on 21/04/2017.
//
//

import Foundation
import Moya
import ObjectMapper

/**
 You should subclass or extend this class in order to make your API calls backed with Moya/Alamofire and returning ObjectMapper models
 */
open class PiOSMoyaNetworkClient<Target> where Target : TargetType {
    /// A MoyaProvider initialized with a given Target implementing TargetType for your API
    var provider: MoyaProvider<Target>
    
    /**
     NetworkClient initializer.
     - parameter provider: your API's MoyaProvider
     */
    public init(provider: MoyaProvider<Target>) {
        self.provider = provider
    }

    /**
     An Array Request.
     - parameter target: your API Target
     - parameter success: a success closure to be executed when the request is successfull and that will initialize a result array of Mappable objects of the given type
     - parameter failure: a closure to be executed on failure and that will give the error that caused the failure
     - returns a Cancellable instance so the pending request can be cancelled during execution
     */
   open func requestArray<T: Mappable>(target: Target, success: @escaping (_ result: [T]) -> Void, failure: @escaping (_ error: MoyaNetworkClientError) -> Void) -> Cancellable {
        let cancellable = self.requestJSON(target: target, success: { (json) -> Void in
            if let error = self.jsonResponseAsError(json: json) {
                failure(error)
                return
            }
            
            if let result:[T] = Mapper<T>().mapArray(JSONObject: json) {
                success(result)
            } else {
                failure(MoyaNetworkClientError.InvalidMapping(json: json))
            }
        }, failure: { (error) -> Void in
            failure(error)
        })
        
        return cancellable
    }
    
    /**
     An Object Request.
     - parameter target: your API Target
     - parameter success: a success closure to be executed when the request is successfull and that will initialize a single Mappable object of the given type
     - parameter failure: a closure to be executed on failure and that will give the error that caused the failure
     - returns a Cancellable instance so the pending request can be cancelled during execution
     */
   open func requestObject<T: Mappable>(target: Target, success: @escaping (_ result: T) -> Void, failure: @escaping (_ error: MoyaNetworkClientError) -> Void) -> Cancellable {
        let cancellable = self.requestJSON(target: target, success: { (json) -> Void in
            if let error = self.jsonResponseAsError(json: json) {
                failure(error)
                return
            }
            
            if let result = Mapper<T>().map(JSONObject: json) {
                success(result)
            } else {
                failure(MoyaNetworkClientError.InvalidMapping(json: json))
            }
        }, failure: { (error) -> Void in
            failure(error)
        })
        
        return cancellable
    }
    
    /**
     A JSON Request.
     - parameter target: your API Target
     - parameter success: a success closure to be executed when the request is successfull and that will return Any json result
     - parameter failure: a closure to be executed on failure and that will give the error that caused the failure
     - returns a Cancellable instance so the pending request can be cancelled during execution
     */
    func requestJSON(target: Target, success: @escaping (_ json: Any) -> Void, failure: @escaping (_ error: MoyaNetworkClientError) -> Void) -> Cancellable {
        let cancellable = self.provider.request(target, completion: { result in
            switch result {
            case let .success(response) where 200..<400 ~= response.statusCode:
                do {
                    let json = try response.mapJSON()
                    success(json)
                } catch (let error) {
                    failure(MoyaNetworkClientError.JSONParsing(cause: error))
                }
            case let .success(response): // Success with status >= 400
                do {
                    let json = try response.mapJSON()
                    failure(MoyaNetworkClientError.RequestError(json: json))
                } catch (let error) {
                    failure(MoyaNetworkClientError.JSONParsing(cause: error))
                }
            case let .failure(error):
                failure(MoyaNetworkClientError.RequestFailure(cause: error))
            }
        })
        return cancellable
    }
    
    /**
     Transforms json containing an error message into an Error.
     - parameter json: the json containing the error
     - returns the appropriate MoyaNetworkClientError
     */
    func jsonResponseAsError(json: Any) -> MoyaNetworkClientError? {
        if let result: ErrorResult = Mapper<ErrorResult>().map(JSONObject: json) {
            if result.isValidError() {
                return self.buildNetworkError(with: result)
            }
        }
        return nil
    }
    
    /**
     Builds the appropriate Error from an ErrorResult.
     - parameter errorResult: the ErrorResult Mapped object
     - returns a MoyaNetworkClientError
     */
    func buildNetworkError(with errorResult: ErrorResult) -> MoyaNetworkClientError {
        let errorMsg = errorResult.description
        let userInfo = [
            NSLocalizedDescriptionKey: errorMsg
        ]
        let error = NSError(domain: "PiOSNetworkErrorDomain", code: 1001, userInfo: userInfo)
        let bufferError = MoyaNetworkClientError.RequestFailure(cause: error)
        return bufferError
    }
}

