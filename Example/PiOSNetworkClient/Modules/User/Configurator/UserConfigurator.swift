//
//  UserConfigurator.swift
//  Example
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 MrCloud. All rights reserved.
//

import UIKit

class UserModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? UserViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: UserViewController) {

        let router = UserRouter()

        let presenter = UserPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = UserInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
