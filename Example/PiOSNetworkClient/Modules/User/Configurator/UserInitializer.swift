//
//  UserInitializer.swift
//  Example
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 MrCloud. All rights reserved.
//

import UIKit

class UserModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var UserViewController: UserViewController!

    override func awakeFromNib() {

        let configurator = UserModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: UserViewController)
    }

}
