//
//  UserServiceAPI.swift
//  Pods
//
//  Created by Florian PETIT on 21/04/2017.
//
//

import Foundation
import Moya

// Github public API
enum UserServiceAPI {
    case getUserInfos(username: String)
    case getUserRepos(username: String)
}

// MARK: - TargetType Protocol Implementation
extension UserServiceAPI : TargetType {
    
    var baseURL: URL { return URL(string: "https://api.github.com")! }
    var path: String {
        switch self {
        case .getUserInfos(let username):
            return "/users/\(username)"
        case .getUserRepos(let username):
            return "/users/\(username)/repos"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getUserInfos, .getUserRepos:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .getUserInfos, .getUserRepos:
            return nil
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .getUserInfos, .getUserRepos:
            return URLEncoding()
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getUserInfos, .getUserRepos:
            return "".data(using: .utf8)!
        }
    }
    
    var task: Task {
        switch self {
        case .getUserInfos, .getUserRepos:
            return .request
        }
    }
    
    
    var multipartBody: [MultipartFormData]? {
        switch self {
        case .getUserInfos, .getUserRepos:
            return nil
        }
    }
}
