//
//  UserService.swift
//  PiOSNetworkClient
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import PiOSNetworkClient
import Moya

class UserService: PiOSMoyaNetworkClient<UserServiceAPI> {
    
    init() {
        let provider = MoyaProvider<UserServiceAPI>()
        super.init(provider: provider)
    }
    
    func fetchUser(username: String, success: @escaping (User) -> Void, error: (Swift.Error) -> Void) -> Cancellable {
        
        let target = UserServiceAPI.getUserInfos(username: username)
        return super.requestObject(target: target, success: success) { (error) in
            print(error)
        }
    }
    
    func fetchUserRepos(username: String, success: @escaping ([Repo]) -> Void, error: (Swift.Error) -> Void) -> Cancellable {
        
        let target = UserServiceAPI.getUserRepos(username: username)
        return super.requestArray(target: target, success: success) { (error) in
            print(error)
        }
    }
}
