//
//  UserViewOutput.swift
//  Example
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 MrCloud. All rights reserved.
//

protocol UserViewOutput {

    /**
        @author Florian PETIT
        Notify presenter that view is ready
    */

    func viewIsReady()
    func getUserInfos(username: String)
    func getReposInfosForUser(username: String)
}
