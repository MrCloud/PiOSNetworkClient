//
//  UserViewController.swift
//  Example
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 MrCloud. All rights reserved.
//

import UIKit

class UserViewController: UIViewController, UserViewInput {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var userInfosLabel: UILabel!
    @IBOutlet weak var reposLabel: UILabel!
    
    
    var output: UserViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: UserViewInput
    func setupInitialState() {
        userInfosLabel.text = ""
        reposLabel.text = ""
    }
    
    @IBAction func fetchInfosTapped(_ sender: Any) {
        guard let username = usernameTextField.text else { return }
        output.getUserInfos(username: username)
    }
    
    func showUserInfos(userInfos: String) {
        userInfosLabel.text = userInfos
    }
    
    @IBAction func fetchRepos(_ sender: Any) {
        guard let username = usernameTextField.text else { return }
        output.getReposInfosForUser(username: username)
    }

    func showRepos(reposInfos: [String]) {
        for repoInfos in reposInfos {
            reposLabel.text?.append("\(repoInfos)\n\n")
        }
    }
}

extension UserViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
