//
//  UserViewInput.swift
//  Example
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 MrCloud. All rights reserved.
//

protocol UserViewInput: class {

    /**
        @author Florian PETIT
        Setup initial state of the view
    */

    func setupInitialState()
    func showUserInfos(userInfos: String)
    func showRepos(reposInfos: [String])
}
