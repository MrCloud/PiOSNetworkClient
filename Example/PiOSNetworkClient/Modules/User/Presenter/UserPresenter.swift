//
//  UserPresenter.swift
//  Example
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 MrCloud. All rights reserved.
//

class UserPresenter: UserModuleInput, UserViewOutput, UserInteractorOutput {

    weak var view: UserViewInput!
    var interactor: UserInteractorInput!
    var router: UserRouterInput!

    func viewIsReady() {

    }
    
    func getUserInfos(username: String) {
        interactor.getUser(username: username) { user in
            self.view.showUserInfos(userInfos: user.description)
        }
    }
    
    func getReposInfosForUser(username: String) {
        interactor.getUserRepos(username: username) { (repos) in
            var infos = [String]()
            for repo in repos {
                infos.append(repo.description)
            }
            self.view.showRepos(reposInfos: infos)
        }
    }

}
