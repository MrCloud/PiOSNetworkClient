//
//  Repo.swift
//  PiOSNetworkClient
//
//  Created by Florian PETIT on 28/04/2017.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import ObjectMapper

class Repo: NSObject, Mappable {
    var id : Int?
    var name : String!
    var fullName : String!
    var repoDescription : String?
    var htmlURL : String!
    
    override var description: String {
        return "name: \(name!) \nfullName: \(fullName!) \nhtmlURL: \(htmlURL!)"
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        fullName <- map["full_name"]
        repoDescription <- map["description"]
        htmlURL <- map["html_url"]
    }
}
