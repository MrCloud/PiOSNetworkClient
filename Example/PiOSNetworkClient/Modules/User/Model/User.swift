//
//  User.swift
//  PiOSNetworkClient
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import ObjectMapper

class User: NSObject, Mappable {
    var login : String!
    var id : Int?
    var avatar_url : String?
    var gravatar_id : String?
    var url : String!
    var html_url : String!
    var repos_url : String?
    var type : String?
    var name : String!
    var company : String?
    var blog : String?
    var location : String?
    var email : String!
    var bio : String?
    var public_repos : Int!
    var public_gists : Int?
    var followers : Int?
    var following : Int?
    var created_at : String?
    var updated_at : String?

    override var description : String {
        return "login: \(login!) \nname: \(name!) \nrepos: \(public_repos!) \nurl: \(html_url!)"
    }
    
    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        login <- map["login"]
        id <- map["id"]
        avatar_url <- map["avatar_url"]
        gravatar_id <- map["gravatar_id"]
        url <- map["url"]
        html_url <- map["html_url"]
        repos_url <- map["repos_url"]
        type <- map["type"]
        name <- map["name"]
        company <- map["company"]
        blog <- map["blog"]
        location <- map["location"]
        email <- map["email"]
        bio <- map["bio"]
        public_repos <- map["public_repos"]
        public_gists <- map["public_gists"]
        followers <- map["followers"]
        following <- map["following"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }
}
