//
//  UserInteractorInput.swift
//  Example
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 MrCloud. All rights reserved.
//

import Foundation

protocol UserInteractorInput {
    func getUser(username: String, completion: @escaping (User) -> Void)
    func getUserRepos(username: String, completion: @escaping ([Repo]) -> Void)
}
