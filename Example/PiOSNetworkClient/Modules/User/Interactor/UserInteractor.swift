//
//  UserInteractor.swift
//  Example
//
//  Created by Florian PETIT on 21/04/2017.
//  Copyright © 2017 MrCloud. All rights reserved.
//

class UserInteractor: UserInteractorInput {
    weak var output: UserInteractorOutput!
    
    
    func getUser(username: String, completion: @escaping (User) -> Void) {
       _ = UserService().fetchUser(username: username, success: { (user) in
            completion(user)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func getUserRepos(username: String, completion: @escaping ([Repo]) -> Void) {
       _ = UserService().fetchUserRepos(username: username, success: { (repos) in
            completion(repos)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
}
